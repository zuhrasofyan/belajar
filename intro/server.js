var express = require ('express'),
	app		= express(),
	port 	= 3000;

app.get('/', function (req, res){
	res.sendFile(__dirname + '/client/views/index.html');
});

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/client/js'));

app.listen(port, function(){
	console.log('access from localhost:' + port);
})