//grab the mongoose module
var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var NerdSchema = new Schema({
	name : {type: String, default: ''},
	date : {type: Date, default: Date.now}
});

//defiine our nerd model
//module.exports allows us to pass this to other files when it is called
module.exports = mongoose.model ('Nerd', NerdSchema);