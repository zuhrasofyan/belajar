//Modules ---------------------------------------
var express 	= require('express');
var app 		= express();
var bodyParser 	= require('body-parser');
var methodOverride	= require ('method-override');

//Configuration ---------------------------------------

//config files
var db = require('./config/db');

//set port
var port = process.env.PORT || 8080;

//mongoDB
//uncomment after enter your credential in config/db.js
//mongoose.connect(db.url);

//get all data/stuff of the body (POST) parameters
//parse application/json
app.use(bodyParser.json());

//parse application/vnd.api+json as json
app.use(bodyParser.json({type: 'application/vnd.api+json'}));

//parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true })); 

//override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(methodOverride('X-HTTP-Method-Override')); 

//set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/public')); 


//Routes ---------------------------------------
require('./app/routes')(app); //configure our $routeParams

//Start app ---------------------------------------
//startup app at localhost:8080
app.listen(port);

//tell user
console.log('listening on port: ' + port);

//expose app
exports = module.exports = app;